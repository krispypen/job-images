# frozen_string_literal: true

require 'spec_helper'

context 'installed packages' do
  brew_list = 'brew list --formula -1'
  brew_cask_list = 'brew list --casks -1'

  describe 'dotnet-sdk' do
    it 'can build an an app' do
      with_fixtures do |fixture_dir|
        cmd = command("cd #{fixture_dir}/dotnet-core && dotnet run")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('Hello World!')
      end
    end
  end

  describe 'bazel' do
    it 'is installed and globally active' do
      cmd = command("asdf current #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can build an a binary' do
      with_fixtures do |fixture_dir|
        cmd = command("cd #{fixture_dir}/bazel && bazel build //main:hello-world")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stderr).to match('bazel-bin/main/hello-world')

        cmd = command("cd #{fixture_dir}/bazel && bazel-bin/main/hello-world")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('Hello world')
      end
    end
  end

  describe 'fvm' do
    it 'is installed and globally active' do
      cmd = command("fvm flutter")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can create an app' do
      cmd = command('fvm flutter create my_app')
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match('All done!')
    end
  end

  describe 'flutter' do
    it 'is configured and globally available' do
      cmd = command("flutter")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can create an app' do
      cmd = command('flutter create my_app')
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match('All done!')
    end
  end

  describe 'rust' do
    it 'is installed and globally active' do
      cmd = command("asdf current #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can compile hello world' do
      with_fixtures do |fixture_dir|
        cmd = command("cd #{fixture_dir}/rust && rustc hello.rs")
        expect(cmd).to be_a_successful_cmd

        cmd = command("cd #{fixture_dir}/rust && ./hello")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('Hello World!')
      end
    end
  end

  describe 'golang' do
    it 'is installed and globally active' do
      cmd = command("asdf current #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can compile hello world' do
      with_fixtures do |fixture_dir|
        cmd = command("go run #{fixture_dir}/go/main.go")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('Hello, world!')
      end
    end
  end

  describe 'gradle' do
    it 'is installed and globally active' do
      cmd = command("asdf current #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can compile an app' do
      with_fixtures do |fixture_dir|
        cmd = command("cd #{fixture_dir}/gradle && gradle compileJava")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('BUILD SUCCESSFUL')

        file = file("#{fixture_dir}/gradle/build/classes/java/main/demo/App.class")
        expect(file).to be_file
      end
    end
  end

  describe 'maven' do
    it 'is installed and globally active' do
      cmd = command("asdf current #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can compile an app' do
      with_fixtures do |fixture_dir|
        cmd = command("cd #{fixture_dir}/maven && mvn compile")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('BUILD SUCCESS')

        file = file("#{fixture_dir}/maven/target/classes/com/mycompany/app/App.class")
        expect(file).to be_file
      end
    end
  end

  describe 'nodejs' do
    it 'is installed and globally active' do
      cmd = command("which node")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can execute hello world' do
      with_fixtures do |fixture_dir|
        cmd = command("node #{fixture_dir}/node/hello.js")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('hello, world!')
      end
    end
  end

  describe 'ruby' do
    it 'is installed and globally active' do
      cmd = command("asdf current #{subject}")
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match('/Users/gitlab/.tool-versions')
    end

    it 'can execute hello world' do
      with_fixtures do |fixture_dir|
        cmd = command("ruby #{fixture_dir}/ruby/hello.rb")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('hello, world!')
      end
    end
  end

  describe 'java' do    
    it 'is installed and globally active' do
      cmd = command("asdf current #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    # JAVA_HOME should be set
    it 'should set $JAVA_HOME' do
      # asdf_update_java_home is called manually because zsh precmd 
      # hooks aren't invoked when running via serverspec
      cmd = command('asdf_update_java_home && printenv')
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match(%r{^JAVA_HOME=/Users/gitlab/.asdf/installs/java/openjdk.*$})
    end

    it 'can execute hello world' do
      with_fixtures do |fixture_dir|
        cmd = command("java #{fixture_dir}/java/hello.java")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('Hello, World!')
      end
    end
  end

  describe 'python' do
    it 'is installed and globally active' do
      cmd = command("asdf current #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can execute hello world' do
      with_fixtures do |fixture_dir|
        cmd = command("python3 #{fixture_dir}/python/hello3.py")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('hello from python 3')

        default = '3'

        cmd = command("python #{fixture_dir}/python/hello#{default}.py")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match("hello from python #{default}")
      end
    end
  end

  describe 'google-cloud-sdk' do
    it 'is installed and globally active' do
      cmd = command("#{brew_cask_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can install components' do
      cmd = command('gcloud components remove cbt --quiet')
      expect([0,1]).to include(cmd.exit_status)
      cmd = command('gcloud components install cbt --quiet')
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stderr).to match('Installing: Cloud Bigtable Command Line Tool')
    end
  end

  describe 'git' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'should allow you to commit' do
      with_tmpdir do |tmpdir|
        cmd = command("cd #{tmpdir} && touch file")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git init .")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git add file")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git config user.email 'you@example.com'")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git config user.name 'Your Name'")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git commit -am test")
        expect(cmd).to be_a_successful_cmd
      end
    end
  end

  describe 'coreutils' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'works' do
      cmd = command('gls /Users/gitlab')
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match('Documents')
    end
  end

  describe 'gcc' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'compiles a C file' do
      with_fixtures do |fixture_dir|
        cmd = command("gcc #{fixture_dir}/gcc/hello.c -o hello")
        expect(cmd).to be_a_successful_cmd

        cmd = command('./hello')
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('Hello, World!')
      end
    end
  end

  describe 'carthage' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'should install a Cartfile' do
      with_fixtures do |fixture_dir|
        cmd = command("cd #{fixture_dir}/carthage && carthage update --no-build")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match(/Checking out AFNetworking/)

        file = file("#{fixture_dir}/carthage/Cartfile.resolved")
        expect(file).to be_file
        expect(file.content).to match('AFNetworking')
      end
    end
  end

  describe 'curl' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'should get google' do
      cmd = command('curl https://www.google.com/generate_204')
      expect(cmd).to be_a_successful_cmd
    end
  end

  describe 'git-lfs' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'should track files' do
      with_tmpdir do |tmpdir|
        cmd = command("cd #{tmpdir} && dd if=/dev/zero of=file.iso count=1024 bs=1024")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git init .")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git lfs install")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git lfs track '*.iso'")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git add file.iso")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git config --global user.email 'you@example.com'")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git config --global user.name 'Your Name'")
        expect(cmd).to be_a_successful_cmd
        cmd = command("cd #{tmpdir} && git commit -am test")
        expect(cmd).to be_a_successful_cmd

        cmd = command("cd #{tmpdir} && git lfs ls-files")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('file.iso')
      end
    end
  end

  describe 'wget' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'should get google' do
      cmd = command('wget https://www.google.com/generate_204')
      expect(cmd).to be_a_successful_cmd
    end
  end

  describe 'openssl' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'is able to show a certificate' do
      with_fixtures do |fixture_dir|
        brew_prefix = machine_is_arm? ? '/opt/homebrew' : '/usr/local'
        cmd = command("#{brew_prefix}/opt/openssl/bin/openssl x509 -in #{fixture_dir}/openssl/certificate.crt -text")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout.gsub(/ /, '')).to match('CN=serverspec')
      end
    end
  end

  describe 'jq' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can parse json' do
      with_fixtures do |fixture_dir|
        cmd = command("jq -r '.foo' #{fixture_dir}/jq/test.json")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match('bar')
      end
    end
  end

  describe 'gpg' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can import key' do
      with_fixtures do |fixture_dir|
        cmd = command("gpg --import #{fixture_dir}/gpg/public.key")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stderr).to match('Total number processed: 1')
      end
    end
  end

  describe 'sshpass' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can provide password to connect to localhost' do
      password = ENV.fetch('LOGIN_PASSWORD', 'gitlab')
      cmd = command("sshpass -p #{password} ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null gitlab@localhost uname")
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match('Darwin')
    end
  end

  describe 'powershell' do
    it 'is installed' do
      cmd = command("#{brew_cask_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can execute commands' do
      cmd = command("pwsh -Command '$PSVersionTable'")
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match('PSVersion')
    end
  end

  describe 'htop' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can run help command' do
      cmd = command("htop -h")
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match('Hisham Muhammad')
    end
  end

  describe 'awscli' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can download a public file' do
      with_tmpdir do |tmpdir|
        cmd = command("aws --region us-east-1 s3 cp s3://gitlab-runner-custom-autoscaler-downloads/v0.5.0/autoscaler-linux-amd64 #{tmpdir} --no-sign-request")
        expect(cmd).to be_a_successful_cmd
        expect(file("#{tmpdir}/autoscaler-linux-amd64")).to be_file
      end
    end
  end

  describe 'xcbeautify' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'should format xcodebuild output' do
      with_fixtures do |fixture_dir|
        cmd = command("cat #{fixture_dir}/xcpretty/xcodebuild.log | xcbeautify")
        expect(cmd).to be_a_successful_cmd
        expect(cmd.stdout).to match(/Test Succeeded/)
      end
    end
  end

  describe 'xcodegen' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'can run xcodegen command' do
      cmd = command("xcodegen -h")
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match('Generate an Xcode project from a spec')
    end
  end

  describe 'imagemagick' do
    it 'is installed' do
      cmd = command("#{brew_list} #{subject}")
      expect(cmd).to be_a_successful_cmd
    end

    it 'the convert command is installed' do
      cmd = command("convert -version")
      expect(cmd).to be_a_successful_cmd
      expect(cmd.stdout).to match('ImageMagick')
    end
  end
end
