#!/bin/bash -e -o pipefail

echo 'Install Command Line Tools'

echo "Mounting image..."
hdiutil attach ./provision/xcode-clt.dmg

echo "Installing package..."
sudo installer -package /Volumes/Command\ Line\ Developer\ Tools/Command\ Line\ Tools.pkg -target /

echo "Unmounting image..."
hdiutil detach /Volumes/Command\ Line\ Developer\ Tools